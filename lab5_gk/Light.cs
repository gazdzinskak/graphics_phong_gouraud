﻿using OpenTK;

namespace lab5_gk
{
    public class BaseLight
    {
        public Vector3 Color = new Vector3(0.4f, 1.0f, 0.4f);
        public float AmbientIntensity = 0.3f;
        public float DiffuseIntensity = 1.0f;
        public void SetColor(float r, float g, float b){Color = new Vector3(r,g,b);}
    }
    public class PointLight : BaseLight
    {
        public Vector3 Position = new Vector3(0.0f, 0.0f, 0.0f);
        public Attenuation LightAttenuation = new Attenuation();
    }
    public class Attenuation
    {
        public float Constant = 1.0f;
        public float Linear = 0.1f;
        public float Exp = 0.1f;
    }
}
