﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using OpenTK.Graphics.OpenGL;

//namespace lab5_gk
//{


//    class TextureRenderer
//    {
//       public int glColorTexture, glDepthTexture, glFBOHandle;
//        int size_w = 1024, size_h = 768;

//        public void Load()
//        {
//            GL.Enable(EnableCap.DepthTest);
//            GL.Enable(EnableCap.Texture2D);

//            CreateTextures();

//            CreateFBOandAssignTexture();

//            Console.WriteLine("Press F for fast clock, S for slow clock, Space to reseed textures");
//        }

//        private void CreateFBOandAssignTexture()
//        {
//            // create and bind an FBO
//            GL.Ext.GenFramebuffers(1, out glFBOHandle);
//            GL.Ext.BindFramebuffer(FramebufferTarget.DrawFramebuffer, glFBOHandle);

//            // assign texture to FBO
//            GL.Ext.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, glColorTexture, 0);
//            GL.Ext.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, glDepthTexture, 0);

//            #region Test for Error



//            switch (GL.Ext.CheckFramebufferStatus(FramebufferTarget.FramebufferExt))
//            {
//                case FramebufferErrorCode.FramebufferCompleteExt:
//                    {
//                        Console.WriteLine("FBO: The framebuffer " + glFBOHandle + " is complete and valid for rendering.");
//                        break;
//                    }
//                case FramebufferErrorCode.FramebufferIncompleteAttachmentExt:
//                    {
//                        Console.WriteLine("FBO: One or more attachment points are not framebuffer attachment complete. This could mean there’s no texture attached or the format isn’t renderable. For color textures this means the base format must be RGB or RGBA and for depth textures it must be a DEPTH_COMPONENT format. Other causes of this error are that the width or height is zero or the z-offset is out of range in case of render to volume.");
//                        break;
//                    }
//                case FramebufferErrorCode.FramebufferIncompleteMissingAttachmentExt:
//                    {
//                        Console.WriteLine("FBO: There are no attachments.");
//                        break;
//                    }
//                /* case  FramebufferErrorCode.GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT: 
//                     {
//                         Console.WriteLine("FBO: An object has been attached to more than one attachment point.");
//                         break;
//                     }*/
//                case FramebufferErrorCode.FramebufferIncompleteDimensionsExt:
//                    {
//                        Console.WriteLine("FBO: Attachments are of different size. All attachments must have the same width and height.");
//                        break;
//                    }
//                case FramebufferErrorCode.FramebufferIncompleteFormatsExt:
//                    {
//                        Console.WriteLine("FBO: The color attachments have different format. All color attachments must have the same format.");
//                        break;
//                    }
//                case FramebufferErrorCode.FramebufferIncompleteDrawBufferExt:
//                    {
//                        Console.WriteLine("FBO: An attachment point referenced by GL.DrawBuffers() doesn’t have an attachment.");
//                        break;
//                    }
//                case FramebufferErrorCode.FramebufferIncompleteReadBufferExt:
//                    {
//                        Console.WriteLine("FBO: The attachment point referenced by GL.ReadBuffers() doesn’t have an attachment.");
//                        break;
//                    }
//                case FramebufferErrorCode.FramebufferUnsupportedExt:
//                    {
//                        Console.WriteLine("FBO: This particular FBO configuration is not supported by the implementation.");
//                        break;
//                    }
//                default:
//                    {
//                        Console.WriteLine("FBO: Status unknown. (yes, this is really bad.)");
//                        break;
//                    }
//            }

//            #endregion Test for Error
//            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // disable rendering into the FBO
//        }


//        private void CreateTextures()
//        {
//            // load texture 
//            GL.GenTextures(1, out glColorTexture);

//            // Still required else TexImage2D will be applyed on the last bound texture
//            GL.BindTexture(TextureTarget.Texture2D, glColorTexture);

//            // generate null texture
//            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, size_w, size_h, 0,
//            OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);

//            // set filtering to nearest so we get "atari 2600" look
//            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
//            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

//            //Create depth texture
//            GL.GenTextures(1, out glDepthTexture);
//            GL.BindTexture(TextureTarget.Texture2D, glDepthTexture);
//            GL.TexImage2D(TextureTarget.Texture2D, 0, (PixelInternalFormat)All.DepthComponent32, size_w, size_h, 0, PixelFormat.DepthComponent, PixelType.UnsignedInt, IntPtr.Zero);
//            // things go horribly wrong if DepthComponent's Bitcount does not match the main Framebuffer's Depth
//            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
//            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
//            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
//            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
//            // GL.Ext.GenerateMipmap( GenerateMipmapTarget.Texture2D );

//        }


//        public void Begin()
//        {
//            GL.Ext.BindFramebuffer(FramebufferTarget.DrawFramebuffer, glFBOHandle);
//            //    GL.PushAttrib(AttribMask.ViewportBit);
//            {
//                GL.Viewport(0, 0, size_w, size_h);

//                // clear the screen in green, to make it very obvious what the clear affected. only the FBO, not the real framebuffer
//                GL.ClearColor(0.0f, 1.0f, 0.0f, 1.0f);
//                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit );

//                // RenderRandomPoints();
//            }
//            // GL.PopAttrib();


//        }

//        public void End()
//        {
//            GL.Ext.BindFramebuffer(FramebufferTarget.ReadFramebuffer, glFBOHandle); // disable rendering into the FBO
//            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // disable rendering into the FBO
//        }
//    }
//}
