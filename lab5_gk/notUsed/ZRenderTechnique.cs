﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using OpenTK;
//using OpenTK.Graphics.OpenGL;

//namespace lab5_gk
//{
//    class ZRenderTechnique
//    {
//        private int glUniformTexture;
//        private int glUniformModelMatrix;
//        private ShaderProgram screenShader;
//        private Model ScreenQuad;
        
//        public void Init()
//        {
//            if (screenShader != null)
//                screenShader.clear();

//            screenShader = new ShaderProgram("pass_fs.glsl", "pass_vs.glsl");
//            screenShader.initProgram();
//            screenShader.Enable();

//            glUniformTexture = screenShader.GetUniformLocation("screenTexture");
//            glUniformModelMatrix = screenShader.GetUniformLocation("modelMatrix");     

//            int []indices = new int[6];
//            indices[0] = 0;
//            indices[1] = 1;
//            indices[2] = 2;
//            indices[3] = 1;
//            indices[4] = 3;
//            indices[5] = 2;

//            Vector3[] positionsdata = new Vector3[4];
//            Vector3[] coldata= new Vector3[4];
//            Vector3[] normals= new Vector3[4];

//            positionsdata[0] = new Vector3(-1.0f, -1.0f, 0.0f);
//            positionsdata[1] = new Vector3(1.0f, -1.0f, 0.0f);
//            positionsdata[2] = new Vector3(-1.0f, 1.0f, 0.0f);
//            positionsdata[3] = new Vector3(1.0f, 1.0f, 0.0f);

//            coldata[0] = new Vector3(1.0f, 0.0f, 0.0f);
//            coldata[1] = new Vector3(1.0f, 0.0f, 0.0f);
//            coldata[2] = new Vector3(1.0f, 0.0f, 0.0f);
//            coldata[3] = new Vector3(1.0f, 0.0f, 0.0f);

          
//            normals[0] = new Vector3(0f, 0f, 0f);
//            normals[1] = new Vector3(1f, 0f, 0f);
//            normals[2] = new Vector3(0f, 1f, 0f);
//            normals[3] = new Vector3(1f, 1f, 0f);


//            ScreenQuad = new Model();
          
//            ScreenQuad.SetIndices(indices);
//            ScreenQuad.SetNormals(normals);
//            ScreenQuad.SetVertices(positionsdata);
//            ScreenQuad.SetColor(new Vector3(1.0f));
//            ScreenQuad.InitGLBuffers();

//            ScreenQuad.SetPosition(new Vector3(0.5f, 0.5f, 0.0f));
//            ScreenQuad.SetScale(new Vector3(0.5f, 0.5f, 1.0f));
//        }

//        public void SetPosition(Vector3 pos)
//        {
//            ScreenQuad.SetPosition(pos);
//        }

//        public void SetScale(Vector3 scale)
//        {
//            ScreenQuad.SetScale(scale);
//        }

//        public void Enable()
//        {
//            screenShader.Enable();
//        }

//        public void SetTexture(int texture)
//        {
//            screenShader.SetUniform(texture, glUniformTexture);
//        }

//        public void Render()
//        {
//            screenShader.SetUniform(ScreenQuad.ModelMatrix(), glUniformModelMatrix);
//            ScreenQuad.Render();
//        }

//    }
//}
