﻿#version 330
 
in vec4 ColorFS;

out vec4 outputColor;
uniform sampler2D screenTexture;
in vec2 TexCoordsFS;


void main()
{
	outputColor =  texture(screenTexture, TexCoordsFS);
}

