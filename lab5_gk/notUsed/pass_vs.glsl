﻿#version 330

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec3 vColor;
layout(location = 2) in vec3 vNormal;

out vec4 ColorFS;
out vec2 TexCoordsFS;

uniform mat4 modelMatrix;

void main()
{
	gl_Position = modelMatrix*vec4(vPosition, 1.0);
	ColorFS = vec4(vColor, 1.0);
	TexCoordsFS = vec2(vNormal.x, vNormal.y);
}
