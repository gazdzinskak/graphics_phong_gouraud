﻿using OpenTK;

namespace lab5_gk
{
    public class Technique
    {
        public ShaderProgram Shader;
        private int glUniformViewMatrix;
        private int glUniformNormalMatrix;
        private int glUniformModelMatrix;
        private int glUniformEyeWorldPosLocation;
        private int glUniformMatSpecularIntensityLocation;
        private int glUniformMatSpecularPowerLocation;
        private string fShaderName;
        private string vShaderName;
        private PointLightsLocation[] glUniformPointLightsLocations;

        public Technique(string fSName, string vSName)
        {
            fShaderName = fSName;
            vShaderName = vSName;
        }
        public void Init()
        {
            if (Shader != null)
                Shader.Clear();

            Shader = new ShaderProgram(fShaderName, vShaderName);
            Shader.InitProgram();

            Shader.Enable();

            glUniformViewMatrix = Shader.GetUniformLocation("modelview");
            glUniformNormalMatrix = Shader.GetUniformLocation("normalmatrix");
            glUniformModelMatrix = Shader.GetUniformLocation("modelmatrix");
            glUniformEyeWorldPosLocation = Shader.GetUniformLocation("gEyeWorldPos");
            glUniformMatSpecularIntensityLocation = Shader.GetUniformLocation("gMatSpecularIntensity");
            glUniformMatSpecularPowerLocation = Shader.GetUniformLocation("gSpecularPower");
        
            glUniformPointLightsLocations = new PointLightsLocation[2];
            for (int i = 0; i < glUniformPointLightsLocations.Length; ++i)
                glUniformPointLightsLocations[i] = new PointLightsLocation();

            string name;
            for (int i = 0; i < glUniformPointLightsLocations.Length; ++i)
            {
                name = "gPointLights[" + i + "].Base.Color";
                glUniformPointLightsLocations[i].Color = Shader.GetUniformLocation(name);

                name = "gPointLights[" + i + "].Base.AmbientIntensity";
                glUniformPointLightsLocations[i].AmbientIntensity = Shader.GetUniformLocation(name);

                name = "gPointLights[" + i + "].Position";
                glUniformPointLightsLocations[i].Position = Shader.GetUniformLocation(name);

                name = "gPointLights[" + i + "].Base.DiffuseIntensity";
                glUniformPointLightsLocations[i].DiffuseIntensity = Shader.GetUniformLocation(name);

                name = "gPointLights[" + i + "].Atten.Constant";
                glUniformPointLightsLocations[i].AttentuationLoc.Constant = Shader.GetUniformLocation(name);

                name = "gPointLights[" + i + "].Atten.Linear";
                glUniformPointLightsLocations[i].AttentuationLoc.Linear = Shader.GetUniformLocation(name);

                name = "gPointLights[" + i + "].Atten.Exp";
                glUniformPointLightsLocations[i].AttentuationLoc.Exp = Shader.GetUniformLocation(name);
            }
        }
        public void Enable(){Shader.Enable();}
        public void SetSpecular(float intensity, float power)
        {
            Shader.SetUniform(intensity, glUniformMatSpecularIntensityLocation);
            Shader.SetUniform(power, glUniformMatSpecularPowerLocation);
        }
        public void SetMVPMatrix(Matrix4 matrix){Shader.SetUniform(matrix, glUniformViewMatrix);}
        public void SetNormalMatrix(Matrix4 matrix){Shader.SetUniform(matrix, glUniformNormalMatrix);}
        public void SetEyePosition(Vector3 vector){Shader.SetUniform(vector, glUniformEyeWorldPosLocation);}
        public void SetModelMatrix(Matrix4 matrix){Shader.SetUniform(matrix, glUniformModelMatrix);}
        public void SetPointLights(PointLight light1, PointLight light2)
        {
            int i = 0;
            Shader.SetUniform(light1.Color, glUniformPointLightsLocations[i].Color);
            Shader.SetUniform(light1.AmbientIntensity, glUniformPointLightsLocations[i].AmbientIntensity);
            Shader.SetUniform(light1.DiffuseIntensity, glUniformPointLightsLocations[i].DiffuseIntensity);
            Shader.SetUniform(light1.Position, glUniformPointLightsLocations[i].Position);
            Shader.SetUniform(light1.LightAttenuation.Constant, glUniformPointLightsLocations[i].AttentuationLoc.Constant);
            Shader.SetUniform(light1.LightAttenuation.Linear, glUniformPointLightsLocations[i].AttentuationLoc.Linear);
            Shader.SetUniform(light1.LightAttenuation.Exp, glUniformPointLightsLocations[i].AttentuationLoc.Exp);

            i = 1;
            Shader.SetUniform(light2.Color, glUniformPointLightsLocations[i].Color);
            Shader.SetUniform(light2.AmbientIntensity, glUniformPointLightsLocations[i].AmbientIntensity);
            Shader.SetUniform(light2.DiffuseIntensity, glUniformPointLightsLocations[i].DiffuseIntensity);
            Shader.SetUniform(light2.Position, glUniformPointLightsLocations[i].Position);
            Shader.SetUniform(light2.LightAttenuation.Constant, glUniformPointLightsLocations[i].AttentuationLoc.Constant);
            Shader.SetUniform(light2.LightAttenuation.Linear, glUniformPointLightsLocations[i].AttentuationLoc.Linear);
            Shader.SetUniform(light2.LightAttenuation.Exp, glUniformPointLightsLocations[i].AttentuationLoc.Exp);
        }
    }
    class PointLightsLocation
    {
        public int Color;
        public int AmbientIntensity;
        public int DiffuseIntensity;
        public int Position;
        public AttentuationLocation AttentuationLoc = new AttentuationLocation();
    }
    class AttentuationLocation
    {
        public int Constant;
        public int Linear;
        public int Exp;
    }
}