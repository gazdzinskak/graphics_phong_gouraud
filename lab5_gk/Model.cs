﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace lab5_gk
{
    public class Model
    {
        const int PositionAttributeLocation = 0;
        const int ColorAttributeLocation = 1;
        const int NormalAttributeLocation = 2;

        int glVAO;
        int glVBOPosition;
        int glVBOColor;
        int glVBONormals;
        int glIBOElements;

        protected Vector3[] positionsdata;
        protected Vector3[] coldata;
        protected Vector3[] normals;
        protected int[] indicedata;


        private Matrix4 modelMatrix;
        private Matrix4 rotationMatrix;
        private Matrix4 scaleMatrix;
        private Matrix4 translationMatrix;


        public Vector3 CurrentPosition { get; set; }
        public float SpecularIntensity { get; set; }
        public float SpecularPower { get; set; }
             

        public Model()
        {
            modelMatrix = Matrix4.Identity;
            rotationMatrix = Matrix4.Identity;
            scaleMatrix = Matrix4.Identity;
            translationMatrix = Matrix4.Identity;
        }

        public Vector3[] GetNormals() { return normals; }
        public Matrix4 GetModelMatrix() { return modelMatrix; }
        public void SetNormals(Vector3[] normalsArray) { normals = normalsArray; }
        public void SetVertices(Vector3[] verticesArray) { positionsdata = verticesArray; }
        public void SetIndices(int[] indicesArray) { indicedata = indicesArray; }

        public int GetVerticesCount()
        {
            if (positionsdata == null)
                return 0;

            return positionsdata.Length;
        }

        public void SetColor(Vector3 color)
        {
            int n = GetVerticesCount();
            var colArray = new Vector3[n];
            for (int i = 0; i < n; i++)
                colArray[i] = color;

            coldata = colArray;
        }

        public void InitGLBuffers()
        {
            GL.GenBuffers(1, out glVBOPosition);
            GL.GenBuffers(1, out glVBOColor);
            GL.GenBuffers(1, out glVBONormals);
            GL.GenBuffers(1, out glIBOElements);

            GL.GenVertexArrays(1, out glVAO);
            GL.BindVertexArray(glVAO);
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, glVBOPosition);
                GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, new IntPtr(positionsdata.Length * Vector3.SizeInBytes), positionsdata, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(PositionAttributeLocation, 3, VertexAttribPointerType.Float, false, 0, 0);

                GL.BindBuffer(BufferTarget.ArrayBuffer, glVBOColor);
                GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, new IntPtr(coldata.Length * Vector3.SizeInBytes), coldata, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(ColorAttributeLocation, 3, VertexAttribPointerType.Float, true, 0, 0);

                GL.BindBuffer(BufferTarget.ArrayBuffer, glVBONormals);
                GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, new IntPtr(normals.Length * Vector3.SizeInBytes), normals, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(NormalAttributeLocation, 3, VertexAttribPointerType.Float, true, 0, 0);

                GL.BindBuffer(BufferTarget.ElementArrayBuffer, glIBOElements);
                GL.BufferData<int>(BufferTarget.ElementArrayBuffer, new IntPtr(indicedata.Length * sizeof(int)), indicedata, BufferUsageHint.StaticDraw);

                GL.EnableVertexAttribArray(PositionAttributeLocation);
                GL.EnableVertexAttribArray(ColorAttributeLocation);
                GL.EnableVertexAttribArray(NormalAttributeLocation);
            }
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        public void Render()
        {
            GL.BindVertexArray(glVAO);
            GL.DrawElements(BeginMode.Triangles, indicedata.Length, DrawElementsType.UnsignedInt, 0);
            GL.BindVertexArray(0);
        }
        
        private void UpdateModelMatrix(){modelMatrix = translationMatrix * rotationMatrix * scaleMatrix;}

        internal void Move(Vector3 vector) { SetPosition(CurrentPosition + vector); }

        public void SetRotation(Vector3 angles)
        {
            double fi = angles.X;
            float cos = (float)Math.Cos(fi);
            float sin = (float)Math.Sin(fi);
            Matrix4 xRotation = new Matrix4
                (1, 0, 0, 0,
                0, cos, (-1) * sin, 0,
                0, sin, cos, 0,
                0, 0, 0, 1);

            fi = angles.Y;
            cos = (float)Math.Cos(fi);
            sin = (float)Math.Sin(fi);
            Matrix4 yRotation = new Matrix4
                (cos, 0, sin, 0,
                0, 1, 0, 0,
                -sin, 0, cos, 0,
                0, 0, 0, 1);

            fi = angles.Z;
            cos = (float)Math.Cos(fi);
            sin = (float)Math.Sin(fi);
            Matrix4 zRotation = new Matrix4
               (cos, -sin, 0, 0,
                sin, cos, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);

            rotationMatrix = zRotation * yRotation * xRotation;
            UpdateModelMatrix();
        }

        public void SetPosition(Vector3 pos)
        {
            CurrentPosition = pos;

            translationMatrix = new Matrix4
                                     (1, 0, 0, pos.X,
                                     0, 1, 0, pos.Y,
                                     0, 0, 1, pos.Z,
                                     0, 0, 0, 1);
            UpdateModelMatrix();
        }

        public void SetScale(Vector3 scale)
        {
            scaleMatrix = new Matrix4
                                    (scale.X, 0, 0, 0,
                                     0, scale.Y, 0, 0,
                                     0, 0, scale.Z, 0,
                                     0, 0, 0, 1);
            UpdateModelMatrix();
        }

      

      
    }
}
