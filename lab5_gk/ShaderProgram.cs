﻿using System;
using System.IO;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace lab5_gk
{
    public class ShaderProgram
    {
        int glProgramID;
        int glAttributeVShaderCol;
        int glAttributeVShaderPos;
        int glAttributeVShaderNorm;
        int glVertexShaderID;
        int glFragmentShaderID;

        private string vertexShaderName;
        private string fragmentShaderName;

        public ShaderProgram() { }
        public ShaderProgram(string fsName,string vsName)
        {
            vertexShaderName = vsName;
            fragmentShaderName = fsName;
        }
        public void InitProgram()
        {
            glProgramID = GL.CreateProgram();
            LoadShader(vertexShaderName, ShaderType.VertexShader, glProgramID, out glVertexShaderID);
            LoadShader(fragmentShaderName, ShaderType.FragmentShader, glProgramID, out glFragmentShaderID);
            GL.LinkProgram(glProgramID);
            Console.WriteLine(GL.GetProgramInfoLog(glProgramID));
            glAttributeVShaderPos = GL.GetAttribLocation(glProgramID, "vPosition");
            glAttributeVShaderCol = GL.GetAttribLocation(glProgramID, "vColor");
            glAttributeVShaderNorm = GL.GetAttribLocation(glProgramID, "vNormal");
            
            if (glAttributeVShaderPos == -1 || glAttributeVShaderCol == -1 || glAttributeVShaderNorm == -1)
                Console.WriteLine("Error binding attributes");
        }
        public void Clear(){GL.DeleteProgram(glProgramID);}

        public int GetUniformLocation(string uniformName)
        {
            int ret = GL.GetUniformLocation(glProgramID, uniformName);
            if(ret==-1)
                Console.WriteLine("Error binding uniform of name " + uniformName);
            return ret;
        }

        void LoadShader(String filename, ShaderType type, int program, out int address)
        {
            address = GL.CreateShader(type);
            using (StreamReader sr = new StreamReader(filename)){GL.ShaderSource(address, sr.ReadToEnd());}
            GL.CompileShader(address);
            GL.AttachShader(program, address);
            Console.WriteLine(GL.GetShaderInfoLog(address));
        }

        public void Enable(){GL.UseProgram(glProgramID);}

        public void SetUniform(Matrix4 matrix, int uniformLocation){GL.UniformMatrix4(uniformLocation, true, ref matrix);}
        public void SetUniform(Vector4 vec, int uniformLocation){GL.Uniform4(uniformLocation, vec);}
        public void SetUniform(Vector3 vec, int uniformLocation){GL.Uniform3(uniformLocation, vec);}
        public void SetUniform(float val, int uniformLocation){GL.Uniform1(uniformLocation, val);}
        public void SetUniform(int val, int uniformLocation){GL.Uniform1(uniformLocation, val);}
        
    }
}

