﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using OpenTK;
using OpenTK.Input;
using ClearBufferMask = OpenTK.Graphics.OpenGL.ClearBufferMask;
using EnableCap = OpenTK.Graphics.OpenGL.EnableCap;
using GL = OpenTK.Graphics.OpenGL.GL;

namespace lab5_gk
{
    class Game : GameWindow
    {
        //int glVBOView;
        float time = 0.0f;

        private int heartIndex, heartGouraudIndex, cubeIndex, cubeGouraudIndex;
        private List<Model> modelsGouraud;
        private List<Model> modelsPhong;
        private Technique techniquePhong;
        private Technique techniqueGouraud;
        private Camera camera;
        private Camera camera2;
        private Vector2 LastMousePos;
        private PointLight light1;
        private PointLight light2;
        private Camera currentCamera;
        

        public Game() : base(1024, 768, OpenTK.Graphics.GraphicsMode.Default)
        {
            techniqueGouraud = new Technique("gouraud_fs.glsl", "gouraud_vs.glsl");
            techniquePhong = new Technique("phong_fs.glsl", "phong_vs.glsl");

            #region light
            light1 = new PointLight();
            light2 = new PointLight();
            light1.SetColor(1.0f,0.0f,0.0f);
            light2.SetColor(0.0f, 0.0f, 1.0f);
          
            light1.Position = new Vector3(-1.0f, 0.0f, -1.0f);
            light2.Position = new Vector3(1.0f, 0.0f, 1.0f);
            #endregion

            #region models
            modelsGouraud = new List<Model>();
            modelsPhong = new List<Model>();
            
            var cubeImported = ModelImporter.Import("cube.obj").FirstOrDefault();

            cubeImported.SetColor(new Vector3(1.0f, 0.4f, 0.4f));
            cubeImported.SetPosition(new Vector3(0.0f, 0.0f, 0.0f));
            cubeImported.SpecularIntensity = 1.0f;
            cubeImported.SpecularPower = 2.0f;
            modelsGouraud.Add(cubeImported);
            cubeGouraudIndex = modelsGouraud.Count() - 1;

            var cubePhongImported = ModelImporter.Import("cube.obj").FirstOrDefault();

            cubePhongImported.SetColor(new Vector3(1.0f, 0.4f, 0.4f));
            cubePhongImported.SetPosition(new Vector3(-3.0f, 0.0f, -3.0f));
            cubePhongImported.SpecularIntensity = 1.0f;
            cubePhongImported.SpecularPower = 2.0f;
            modelsPhong.Add(cubePhongImported);
            cubeIndex = modelsPhong.Count() - 1;

            var oak = ModelImporter.Import("oak.obj");

            foreach (var model in oak)
            {
                model.SetScale(new Vector3(0.1f, 0.1f, 0.1f));
                model.SetPosition(new Vector3(2.5f, 0.0f, 2.0f));
                model.SetColor(new Vector3(0.5f, 1.0f, 0.5f));
                model.SpecularIntensity = 0f;
                model.SpecularPower = 0f;
                modelsPhong.Add(model);
            }

            var heart = ModelImporter.Import("Heart.obj").FirstOrDefault();

            heart.SetPosition(new Vector3(-2.5f, 0.2f, 0.9f));
            heart.SetColor(new Vector3(1.0f, 0.1f, 0.1f));
            heart.SpecularIntensity = 10f;
            heart.SpecularPower = 100f;
            modelsPhong.Add(heart);
            heartIndex = modelsPhong.Count() - 1;


            var sphere = ModelImporter.Import("sphere2.obj").FirstOrDefault();

            sphere.SetScale(new Vector3(0.6f, 0.6f, 0.6f));
            sphere.SetPosition(new Vector3(-5.0f, 1.0f, 1.0f));
            sphere.SetColor(new Vector3(0.8f, 0.8f, 0.1f));
            sphere.SpecularIntensity = 10f;
            sphere.SpecularPower = 80f;
            modelsPhong.Add(sphere);

            var heartGouraud = ModelImporter.Import("Heart.obj").FirstOrDefault();

            heartGouraud.SetScale(new Vector3(0.01f, 0.01f, 0.01f));
            heartGouraud.SetPosition(new Vector3(1.0f, 0.2f, -2.5f));
            heartGouraud.SetColor(new Vector3(1.0f, 1.0f, 0.1f));
            heartGouraud.SpecularIntensity = 10f;
            heartGouraud.SpecularPower = 100f;
            modelsGouraud.Add(heartGouraud);
            heartGouraudIndex = modelsGouraud.Count() - 1;

            var sphereGouraud = ModelImporter.Import("sphere2.obj").FirstOrDefault();

            sphereGouraud.SetScale(new Vector3(0.6f, 0.6f, 0.6f));
            sphereGouraud.SetPosition(new Vector3(3.0f, 0.0f, -1.0f));
            sphereGouraud.SetColor(new Vector3(0.1f, 0.1f, 1.0f));
            sphereGouraud.SpecularIntensity = 10f;
            sphereGouraud.SpecularPower = 90f;
            modelsGouraud.Add(sphereGouraud);

            var plane = ModelImporter.Import("cube.obj").FirstOrDefault();
            plane.SetScale(new Vector3(10.0f, 0.5f, 10.0f));
            plane.SetPosition(new Vector3(-1.0f, -1.0f, 0.0f));
            plane.SpecularIntensity = 0f;
            plane.SpecularPower = 0f;
            plane.SetColor(new Vector3(1.0f, 1.0f, 0.4f));
            modelsGouraud.Add(plane);

            #endregion

            #region camera
            camera = new Camera();
            camera.SetCameraPosition(-2.0f, 7.0f, 12.0f);
            camera.Orientation = new Vector3(-3.45f, -0.49f, 1.0f);
            LastMousePos = new Vector2();
            camera.SetRatio(ClientSize.Height / (float)ClientSize.Width);

            camera2 = new Camera();
            camera2.SetCameraPosition(-5.0f, 10.0f, -10.0f);
            camera2.Orientation = new Vector3(0.4f, -0.7f, 0.8f);
            camera2.SetRatio(ClientSize.Height / (float)ClientSize.Width);

            currentCamera = camera;
            #endregion
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            techniquePhong.Init();

            foreach (var model in modelsPhong)
                model.InitGLBuffers();
            
            techniqueGouraud.Init();

            foreach (var model in modelsGouraud)
                model.InitGLBuffers();

            Title = "Projekt nr 5";
            GL.ClearColor(Color.DarkOliveGreen);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            GL.Viewport(0, 0, Width, Height);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.DepthTest);

            RenderModels(techniquePhong, modelsPhong);
            RenderModels(techniqueGouraud, modelsGouraud);

            GL.Flush();
            SwapBuffers();
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);

            //move current camera
            var vector = new Vector3(0, 0, 0);
            const float step = 0.05f;

            if (e.Key == Key.A)
                vector.X = -step;
            if (e.Key == Key.D)
                vector.X = step;
            if (e.Key == Key.W)
                vector.Z = -step;
            if (e.Key == Key.S)
                vector.Z = step;

            currentCamera.Move(vector.X, vector.Y, vector.Z);

            //change camera
            if (e.Key == Key.H)
                currentCamera = currentCamera == camera ? camera2 : camera;


            //reload shaders
            if (e.Key == Key.R)
            {
                techniqueGouraud = new Technique("gouraud_fs.glsl", "gouraud_vs.glsl");
                techniquePhong = new Technique("phong_fs.glsl", "phong_vs.glsl");
                techniqueGouraud.Init();
                techniquePhong.Init();
            }

            var vector2 = new Vector3(0, 0, 0);
            if (e.Key == Key.Left)
                vector2.X = -step;
            if (e.Key == Key.Right)
                vector2.X = step;
            if (e.Key == Key.Up)
                vector2.Z = -step;
            if (e.Key == Key.Down)
                vector2.Z = step;
            if (e.Key == Key.PageDown)
                vector2.Y = -step;
            if (e.Key == Key.PageUp)
                vector2.Y = step;

            modelsGouraud[cubeGouraudIndex].Move(vector2);

            var vector3 = new Vector3(0, 0, 0);
            if (e.Key == Key.J)
                vector3.X = -step;
            if (e.Key == Key.L)
                vector3.X = step;
            if (e.Key == Key.K)
                vector3.Z = -step;
            if (e.Key == Key.I)
                vector3.Z = step;
            if (e.Key == Key.M)
                vector3.Y = -step;
            if (e.Key == Key.N)
                vector3.Y = step;

            modelsPhong[cubeIndex].Move(vector3);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            LastMousePos = new Vector2(e.X, e.Y);
            currentCamera.OnMouseDown(e.X, e.Y);
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);
            currentCamera.AddRotation(e.X, e.Y);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            currentCamera.OnMouseUp();
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            time += (float)e.Time;

            light1.Position = new Vector3((float)Math.Sin(time * 0.35d) * 5.0f, 1.0f, (float)Math.Cos(time * 0.35d) * 5.0f);
            light2.Position = new Vector3((float)Math.Cos(time * 0.5d) * 3.0f, 1.0f, (float)Math.Sin(time * 0.5d) * 5.0f);

            float hs1 = 0.2f + 0.05f * Math.Abs((float)Math.Sin(3d * time + Math.Cos(time)));
            float hs2 = 0.2f + 0.05f * Math.Abs((float)Math.Sin(1.0f + 3d * time + Math.Cos(time)));

            modelsPhong[heartIndex].SetScale(new Vector3(hs1, hs1, hs1));
            modelsGouraud[heartGouraudIndex].SetScale(new Vector3(hs2, hs2, hs2));
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);
            currentCamera.SetRatio((float)ClientSize.Height / (float)ClientSize.Width);
        }

        private void RenderModels(Technique technique, List<Model> models)
        {
            technique.Enable();
            technique.SetEyePosition(currentCamera.Position);

            foreach (var model in models)
            {
                technique.SetSpecular(model.SpecularIntensity, model.SpecularPower);
                technique.SetPointLights(light1, light2);
                var mvpmat = currentCamera.GetProjectionMatrix() * currentCamera.GetViewMatrix() * model.GetModelMatrix(); 
                technique.SetMVPMatrix(mvpmat);
                var modelmat = model.GetModelMatrix();
                technique.SetModelMatrix(modelmat);
                var normalMatrix = modelmat;

                technique.SetNormalMatrix(normalMatrix);
                model.Render();
            }
        }


    }
}
