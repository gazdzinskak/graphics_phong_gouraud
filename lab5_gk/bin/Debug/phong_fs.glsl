﻿#version 330
 
const int MAX_POINT_LIGHTS = 2;


in vec3 NormalFS;
in vec3 WorldPositionFS;
in vec4 ColorFS;

out vec4 outputColor;
uniform sampler2D screenTexture;


struct BaseLight
{
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
};

struct Attenuation
{
	float Constant;
	float Linear;
	float Exp;
};

struct PointLight
{
	BaseLight Base;
	vec3 Position;
	Attenuation Atten;
};


 uniform int gNumPointLights = 2;
uniform PointLight gPointLights[MAX_POINT_LIGHTS];
uniform vec3 gEyeWorldPos;
uniform float gMatSpecularIntensity;
uniform float gSpecularPower;

vec4 CalcLightInternal(BaseLight Light, vec3 LightDirection, vec3 NormalFS)
{
    vec4 AmbientColor = vec4(Light.Color, 1.0) * Light.AmbientIntensity;
    float DiffuseFactor = dot(NormalFS, -LightDirection);

    vec4 DiffuseColor = vec4(0, 0, 0, 0);
    vec4 SpecularColor = vec4(0, 0, 0, 0);

    if (DiffuseFactor > 0) {
        DiffuseColor = vec4(Light.Color, 1.0) * Light.DiffuseIntensity * DiffuseFactor;
        vec3 pixelToEye = normalize(gEyeWorldPos - WorldPositionFS);
        vec3 LightReflect = normalize(reflect(LightDirection, NormalFS));
        float SpecularFactor = dot(pixelToEye, LightReflect);
        SpecularFactor = pow(SpecularFactor, gSpecularPower);
        if (SpecularFactor > 0) {
            SpecularColor = vec4(Light.Color, 1.0) *
            gMatSpecularIntensity * SpecularFactor;
        }
    }

    return (AmbientColor + DiffuseColor + SpecularColor);
}

vec4 CalcPointLight(int Index, vec3 NormalFS)
{
    vec3 LightDirection = WorldPositionFS - gPointLights[Index].Position;
    float Distance = length(LightDirection);
    LightDirection = normalize(LightDirection);

    vec4 Color = CalcLightInternal(gPointLights[Index].Base, LightDirection, NormalFS);
    float Attenuation = gPointLights[Index].Atten.Constant +
                        gPointLights[Index].Atten.Linear * Distance +
                        gPointLights[Index].Atten.Exp * Distance * Distance;

    return Color / Attenuation;
}


void
main()
{
	//discard;
	vec3 NormalFS = normalize(NormalFS);
    vec4 TotalLight = vec4(0);//CalcDirectionalLight(NormalFS);

    for (int i = 0 ; i < gNumPointLights ; i++) {
        TotalLight += CalcPointLight(i, NormalFS);
    }
    outputColor = ColorFS*(TotalLight);//*0.01 + vec4(NormalFS, 1.0);
}

