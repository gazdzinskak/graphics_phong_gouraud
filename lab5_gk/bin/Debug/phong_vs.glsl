﻿#version 330
 
layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec3 vColor;
layout (location = 2) in vec3 vNormal;


out vec3 NormalFS;
out vec3 WorldPositionFS;
out vec4 ColorFS;

uniform mat4 modelview;
uniform mat4 normalmatrix;
uniform mat4 modelmatrix;
 
void main()
{
    gl_Position = modelview * vec4(vPosition, 1.0);
	NormalFS = (normalmatrix*vec4(vNormal,0.0)).xyz;
	WorldPositionFS = (modelmatrix*vec4(vPosition,1.0)).xyz;

	ColorFS = vec4( vColor, 1.0);
}
