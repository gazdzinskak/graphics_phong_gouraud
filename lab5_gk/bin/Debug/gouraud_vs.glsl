﻿#version 330
 
const int MAX_POINT_LIGHTS = 2;
layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec3 vColor;
layout (location = 2) in vec3 vNormal;

out vec4 ColorFS;

vec3 NormalFS;
vec3 WorldPosition;

uniform mat4 modelview;
uniform mat4 normalmatrix;
uniform mat4 modelmatrix;

struct BaseLight
{
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
};

struct Attenuation
{
	float Constant;
	float Linear;
	float Exp;
};

struct PointLight
{
	BaseLight Base;
	vec3 Position;
	Attenuation Atten;
};


uniform int gNumPointLights = 2;
uniform PointLight gPointLights[MAX_POINT_LIGHTS];
uniform vec3 gEyeWorldPos;
uniform float gMatSpecularIntensity;
uniform float gSpecularPower;

vec4 CalcLightInternal(BaseLight Light, vec3 LightDirection, vec3 NormalFS)
{
    vec4 AmbientColor = vec4(Light.Color, 1.0) * Light.AmbientIntensity;
    float DiffuseFactor = dot(NormalFS, -LightDirection);

    vec4 DiffuseColor = vec4(0, 0, 0, 0);
    vec4 SpecularColor = vec4(0, 0, 0, 0);

    if (DiffuseFactor > 0) {
        DiffuseColor = vec4(Light.Color, 1.0) * Light.DiffuseIntensity * DiffuseFactor;
        vec3 pixelToEye = normalize(gEyeWorldPos - WorldPosition);
        vec3 LightReflect = normalize(reflect(LightDirection, NormalFS));
        float SpecularFactor = dot(pixelToEye, LightReflect);
        SpecularFactor = pow(SpecularFactor, gSpecularPower);
        if (SpecularFactor > 0) {
            SpecularColor = vec4(Light.Color, 1.0) *
            gMatSpecularIntensity * SpecularFactor;
        }
    }

    return (AmbientColor + DiffuseColor + SpecularColor);
}

vec4 CalcPointLight(int Index, vec3 NormalFS)
{
    vec3 LightDirection = WorldPosition - gPointLights[Index].Position;
    float Distance = length(LightDirection);
    LightDirection = normalize(LightDirection);

    vec4 Color = CalcLightInternal(gPointLights[Index].Base, LightDirection, NormalFS);
    float Attenuation = gPointLights[Index].Atten.Constant +
                        gPointLights[Index].Atten.Linear * Distance +
                        gPointLights[Index].Atten.Exp * Distance * Distance;

    return Color / Attenuation;
}
 
void main()
{
    gl_Position = modelview * vec4(vPosition, 1.0);
	NormalFS = (normalmatrix*vec4(vNormal,0.0)).xyz;
	WorldPosition = (modelmatrix*vec4(vPosition,1.0)).xyz;

	vec3 NormalFS = normalize(NormalFS);
    vec4 TotalLight = vec4(0);

    for (int i = 0 ; i < gNumPointLights ; i++) {
        TotalLight += CalcPointLight(i, NormalFS);
    }
    

    ColorFS = vec4( vColor, 1.0)*(TotalLight);
}
