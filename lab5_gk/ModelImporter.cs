﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Assimp;
using Assimp.Configs;
using OpenTK;

namespace lab5_gk
{
        public class ModelImporter
    {
        public static List<Model> Import(string file)
        {
            String fileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),file);
            
            AssimpContext importer = new AssimpContext();
            
            NormalSmoothingAngleConfig config = new NormalSmoothingAngleConfig(66.0f);
            importer.SetConfig(config);
            
            LogStream logstream = new LogStream(delegate(String msg, String userData) { Console.WriteLine(msg); });
            logstream.Attach();

            Scene model = importer.ImportFile(fileName, PostProcessPreset.TargetRealTimeMaximumQuality);
            
            List<Model >newModels = new List<Model>();
            Model newModel = new Model();
            foreach (var mesh in model.Meshes)
            {
                newModel = new Model();
                newModel.SetIndices(mesh.GetIndices());
                var normals = ConvertVectors(mesh.Normals);
                newModel.SetNormals(normals);
                var vertices = ConvertVectors(mesh.Vertices);
                newModel.SetVertices(vertices);
                newModels.Add(newModel);
            }
            
            importer.Dispose();
            return newModels;
        }

        private static Vector3[] ConvertVectors(List<Vector3D> list)
        {
            Vector3[] array = new Vector3[list.Count];
            int i = 0;
            foreach (var vector3D in list)
                array[i++] = new Vector3(vector3D.X, vector3D.Y, vector3D.Z);

            return array;
        }
    }
}


