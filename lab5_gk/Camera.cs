﻿using OpenTK;
using System;

namespace lab5_gk
{
    public class Camera
    {
        public Vector3 Position = Vector3.Zero;
        public Vector3 Orientation = new Vector3((float)Math.PI, 0f, 0f);
        public float MoveSpeed = 0.2f;
        public float MouseSensitivity = 0.006f;
        public float Ratio = 0.75f;

        public Vector2 CursorLastPosition;

        public Vector3 XDir;
        public Vector3 YDir;
        public Vector3 ZDir;

        private bool IsMouseDown = false;

        //viewMatrix - create look at
        public Matrix4 GetViewMatrix()
        {
            //vector in the direction the camera is looking
            ZDir.X = (float)(Math.Sin(Orientation.X) * Math.Cos(Orientation.Y));
            ZDir.Y = (float)Math.Sin(Orientation.Y);
            ZDir.Z = (float)(Math.Cos(Orientation.X) * Math.Cos(Orientation.Y));

            //Console.WriteLine(Orientation.X + " " + Orientation.Y+" "+Orientation.Z);
            return GetLookAtMatrix(Position, Position + ZDir, Vector3.UnitY);
        }

        public void SetCameraPosition (float x, float y, float z){Position = new Vector3(x,y,z);}

        public Matrix4 GetLookAtMatrix(Vector3 position, Vector3 target, Vector3 up)
        {
            ZDir = position - target;
            ZDir.Normalize();

            XDir = Vector3.Cross(up, ZDir);
            XDir.Normalize();

            YDir = Vector3.Cross(ZDir, XDir);
            YDir.Normalize();

            var dotsX = -Vector3.Dot(position, XDir);
            var dotsY = -Vector3.Dot(position, YDir);
            var dotsZ = -Vector3.Dot(position, ZDir);

            var orientation = new Matrix4
            (
                new Vector4(XDir, dotsX),
                new Vector4(YDir, dotsY),
                new Vector4(ZDir, dotsZ),
                new Vector4(0.0f, 0.0f, 0.0f, 1.0f)
            );
            
            return orientation;
        }

        public void Move(float x, float y, float z)
        {
            Vector3 translation = new Vector3(x, y, z);
            Position += translation.Z * ZDir + translation.X * XDir;
        }

        public void AddRotation(float newX, float newY)
        {
            if(!IsMouseDown) return;

            float deltaX = CursorLastPosition.X - newX;
            float deltaY = CursorLastPosition.Y - newY;

            CursorLastPosition = new Vector2(newX, newY);

            float x = deltaX * MouseSensitivity;
            float y = deltaY * MouseSensitivity;

            Orientation.X = (Orientation.X + x) % ((float)Math.PI * 2.0f);
            Orientation.Y = Math.Max(Math.Min(Orientation.Y + y, (float)Math.PI / 2.0f - 0.1f), (float)-Math.PI / 2.0f + 0.1f);
        }

        public Matrix4 GetProjectionMatrix()
        {
            float fov = (float)Math.PI *0.3f;
            float f = 100.0f;
            float n = 1.0f;

            float e = 1f / ((float)Math.Tan(fov / 2f));
            float a = Ratio;

            return new Matrix4(
                e, 0, 0, 0,
                0, e / a, 0, 0,
                0, 0, -(f + n) / (f - n), -2 * f * n / (f - n),
                0, 0, -1, 0
                );
        }

        public void SetRatio(float ratio) {Ratio = ratio;}
        public void OnMouseDown(float x, float y)
        {
            IsMouseDown = true;
            CursorLastPosition = new Vector2(x, y);
        }
        public void OnMouseUp()
        {
            IsMouseDown = false;
        }
    }
}
